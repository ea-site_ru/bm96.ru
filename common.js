$(document).ready(function() {
    // слайдеры
    var mainpage_slider = new Swiper('#mainpage_slider', {
        centeredSlides: true,
        effect: 'fade',
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });


    // fix против возврата кастомного "плейсхолдера" (.form__input-custom-placeholder) у заполненных полей формы
    $(document).on('change', '.form-input-wrap .form__input', function() {
        ($(this).val() != '') ? $(this).addClass('not-empty') :
            $(this).removeClass('not-empty');
    });
    // \\ fix против возврата кастомного "плейсхолдера" (.form__input-custom-placeholder) у заполненных полей формы


    // сворачивание/разворачивание блоков
    $(document).on('click', '[data-toggle]', function(e) {
        e.preventDefault();

        var btn = $(this),
            target = $( $(this).data('toggle') );

        btn.toggleClass('active');

        if (btn.data('set-active') != '')
            $(btn.data('set-active')).toggleClass('active');

        target.toggleClass('visible');
    });
});


/* действия по успешной отправке формы через AjaxForm */
$(document).on('af_complete', function(event, response) {
    // автоматическое закрытие magnificpopup при успешной отправке формы
    if (response.success)
        $.magnificPopup.close();
});
/* \\ действия по успешной отправке формы через AjaxForm */


/* YANDEX MAPS. Ищет на странице элемент с атрибутом data-map="*адрес*" и вставляет в него карту с маркером на указанном в атрибуте адресом */
if ($('[data-map]').length > 0) {
    (function maps() {
        this.yandexMapsInit = function(c) {
        if(!$(c).length) return false;

        $.getScript('//api-maps.yandex.ru/2.1/?lang=ru_RU', function() {
            ymaps.ready(init);

            function init() {
                $(c).each(function() {
                    var $t = $(this);

                    $t.html('');

                    var address = $t.data('map'),
                        coords;

                    coords = JSON.parse('[' + address + ']');

                    var myMap = new ymaps.Map($t[0], {
                        center: coords,
                        zoom: 16,
                        controls: ['zoomControl']
                    });

                    myMap.geoObjects.add(new ymaps.Placemark(coords, {

                    }, {
                        preset: 'islands#icon'
                    }));

                    myMap.geoObjects.add(firstGeoObject);

                    // отключаем прокрутку карты на экранах уже 768px
                    if ($(document).width() < 768)
                        myMap.behaviors.disable('drag');
                });
            }
        });
        }

        yandexMapsInit('[data-map]');
    })();
}
/* \\ YANDEX MAPS */
