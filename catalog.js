/***
number - исходное число
decimals - количество знаков после разделителя
dec_point - символ разделителя
thousands_sep - разделитель тысячных
***/
function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k).toFixed(prec);
        };

    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
        .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

/* Пример вызова: plural(13, ['год', 'года', 'лет']) // вернёт 'лет' */
function plural(n,f){n%=100;if(n>10&&n<20)return f[2];n%=10;return f[n>1&&n<5?1:n==1?0:2]}

function recalc_prod_sum(prod_id) {
    var count      = parseInt($('#prod_' + prod_id + '_count').val()),
        price_text = $('#prod_' + prod_id + '_price').text(),
        price      = price_text.replace(/[^\d;]/g, '') * 1;
    
    $('#' + prod_id + '_prod_total_amount').text(number_format(count * price, 0, ',', ' '));
}

$(document).ready(function() {
    // слайдеры
    if ( $('#popular_products_slider').length > 0 ) {
        var popular_products_slider = new Swiper('#popular_products_slider', {
            slidesPerView: 4,
            spaceBetween: 20,
            loop: true,
            breakpoints: {
                1199: {
                    slidesPerView: 3,
                    spaceBetween: 15,
                },
                991: {
                    slidesPerView: 2,
                    spaceBetween: 10,
                },
                767: {
                    slidesPerView: 1
                }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }

    if ( $('#similar_products_slider.swiper-container').length > 0 ) {
        var similar_products_slider = new Swiper('#similar_products_slider', {
            slidesPerView: 5,
            spaceBetween: 20,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            loop: true,
            breakpoints: {
                1199: {
                    slidesPerView: 4,
                    spaceBetween: 15,
                },
                991: {
                    slidesPerView: 3,
                    spaceBetween: 10,
                },
                767: {
                    slidesPerView: 1
                }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
        // остановка автопроигрывания слайдера при наведении на него
        $('#similar_products_slider').on('mouseenter', function(){
            similar_products_slider.autoplay.stop();
        });
        $('#similar_products_slider').on('mouseleave', function(){
            similar_products_slider.autoplay.start();
        })
        // \\ остановка автопроигрывания слайдера при наведении на него
    }

    if ( $('.prod-page__gallery-thumbs.swiper-container').length > 0 ) {
        var product_page_gallery_thumbs = new Swiper('.prod-page__gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 3,
            direction: 'vertical',
            loop: false,
            grabCursor: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            navigation: {
                nextEl: '.swiper-button-bottom',
                prevEl: '.swiper-button-top',
            },
        });
        var product_page_gallery = new Swiper('.prod-page__gallery-desktop', {
            spaceBetween: 5,
            loop: false,
            grabCursor: true,
            thumbs: {
                swiper: product_page_gallery_thumbs,
            },
        });
    }


    // переключение слайдера на странице товара при клике на превьюшки
    $(document).on('click', '[data-to-slide]', function() {
        product_page_gallery.slideToLoop( $(this).data('to-slide') );
    });
    // \\ переключение слайдера на странице товара при клике на превьюшки
    // \\ слайдеры


    // счётчик товара
    var timeout;
    $(document).on('click', '[data-count-change]', function(e) {
        e.preventDefault();

        var count_field = $(this).siblings('[name="count"]'),
            prod_id     = count_field.data('prod-id');

        if (
            ( parseInt($(this).data('count-change')) == 1  && count_field.val() >= 1 && count_field.val() < parseInt(count_field.data('maxvalue')) ) ||
            ( parseInt($(this).data('count-change')) == -1 && count_field.val() > 1 && count_field.val() <= parseInt(count_field.data('maxvalue')) )
        ) {
            count_field.val( parseInt(count_field.val()) + parseInt($(this).data('count-change')) );

            // добавление товара в корзину при изменении количества
            if ( $('.cart-page').length ) {
                count_field.trigger('submit');
                
                // пересчитываем сумму по товару
                recalc_prod_sum(prod_id);
            }
        }
    });

    var cur_count_val, count_field;

    $('[name="count"]').focus(function() {
        count_field = $(this);
        cur_count_val = count_field.val();
    });

    // запрет ввода не цифр
    $(document).on('change keypress keyup blur', '[name="count"]', function(e) {        
        var val = $(this).val(),
            input = $(this),
            prod_id = input.data('prod-id');

        if ( e.type == 'focusout' && $('#' + e.target.id).val() == '' )
            $('#' + e.target.id).val(cur_count_val);

        // не даём ввести число больше остатка товара в наличии
        if ( parseInt(val) > parseInt($(this).data('maxvalue')) )
            $(this).val($(this).data('maxvalue'));

        if ( $('.cart-page').length ) {
            if ( val != '' )
                input.trigger('submit');

            // пересчитываем сумму по товару
            recalc_prod_sum(prod_id);
        }
    });


    // при изменении товара в корзине меняем "товар" на "товара" / "товаров" и т.д.
    $(document).on('DOMSubtreeModified', '.ms2_total_count', function() {
        $('#cart_info_plural_product_text').text( plural(parseInt( $(this).text() ), ['товар', 'товара', 'товаров']) );
    });
    // \\ при изменении товара в корзине меняем "товар" на "товара" / "товаров" и т.д.


    // отображение блока для ввода адреса доставки при выборе доставки во время оформления заказа
    if ( $('[name="delivery"]:checked').val() == 2 ) // 1 - самовывоз, 2 - доставка
        $('#order_page_delivery_address').show();

    $(document).on('click', '[name="delivery"]', function() {
        var $delivery_id = $(this).val();

        if ($delivery_id == 2) // 1 - самовывоз, 2 - доставка
            $('#order_page_delivery_address').show();
        else
            $('#order_page_delivery_address').hide();
    });
    // \\ отображение блока для ввода адреса доставки при выборе доставки во время оформления заказа
});